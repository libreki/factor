<!--SPDX-License-Identifier: FSFAP-->

# Factor CAS

> Copyright © 2022-2023 libreki
>
> Copying and distribution of this file, with or without modification,
> are permitted in any medium without royalty provided the copyright
> notice and this notice are preserved.  This file is offered as-is,
> without any warranty.

Factor CAS is a GTK 4 frontend for the Giac CAS library.

## Compiling and running

### Requirements

- [GTK 4](https://www.gtk.org/)
- [Libadwaita](https://gnome.pages.gitlab.gnome.org/libadwaita/)
- [Giac](https://www-fourier.univ-grenoble-alpes.fr/~parisse/giac.html)

### Instructions

```
$ git clone https://codeberg.org/libreki/factor.git
$ cd factor
$ make
```

To run the program uninstalled, use:

```
$ make run
```

To install or subsequently uninstall the program, use:

```
$ sudo make install
$ sudo make uninstall
```
