/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Factor CAS.
 *
 *  Factor CAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Factor CAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Factor CAS.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "cgiac.h"

#include <giac/config.h>
#include <giac/giac.h>

static const int ENGLISH = 1;

giac_context *
giac_context_new (void)
{
  return reinterpret_cast<giac_context *> (new giac::context ());
}

void
giac_context_delete (giac_context *ct)
{
  delete reinterpret_cast<giac::context *> (ct);
}

giac_vector_aide *
giac_read_help (void)
{
  int count;
  std::vector<giac::aide> help = giac::readhelp (giac::default_helpfile, count);
  return reinterpret_cast<giac_vector_aide *> (new std::vector<giac::aide> (help));
}

void
giac_vector_aide_delete (giac_vector_aide *help)
{
  delete reinterpret_cast<std::vector<giac::aide> *> (help);
}

const char **
giac_get_commands (giac_vector_aide *help)
{
  std::vector<giac::aide> *_help = reinterpret_cast<std::vector<giac::aide> *> (help);

  const char **commands = static_cast<const char **> (malloc ((_help->size () + 1)
                                                              * sizeof (char *)));

  for (unsigned int i = 0; i < _help->size (); i++)
    commands[i] = _help->at (i).cmd_name.c_str ();

  commands[_help->size ()] = NULL;
  return commands;
}

static giac_aide *
cc_aide_to_c (const giac::aide &cc)
{
  giac_aide *aide = static_cast<giac_aide *> (malloc (sizeof (giac_aide)));

  aide->cmd_name = strdup (cc.cmd_name.c_str ());

  if (cc.blabla.size () >= ENGLISH + 1)
    aide->explanation = strdup (cc.blabla.at (ENGLISH).chaine.c_str ());
  else
    aide->explanation = strdup ("");

  aide->examples_length = cc.examples.size ();
  aide->examples = static_cast<char **> (malloc (aide->examples_length * sizeof (char *)));
  for (int i = 0; i < aide->examples_length; i++)
    aide->examples[i] = strdup (cc.examples[i].c_str ());

  aide->related_length = cc.related.size ();
  aide->related = static_cast<char **> (malloc (aide->related_length * sizeof (char *)));
  for (int i = 0; i < aide->related_length; i++)
    aide->related[i] = strdup (cc.related[i].chaine.c_str ());

  aide->synonyms_length = cc.synonymes.size ();
  aide->synonyms = static_cast<char **> (malloc (aide->synonyms_length * sizeof (char *)));
  for (int i = 0; i < aide->synonyms_length; i++)
    aide->synonyms[i] = strdup (cc.synonymes[i].chaine.c_str ());

  aide->syntax = strdup (cc.syntax.c_str ());

  return aide;
}

giac_aide *
giac_help_on (const char       *command,
              giac_vector_aide *help)
{
  std::vector<giac::aide> *_help = reinterpret_cast<std::vector<giac::aide> *> (help);

  giac::aide _aide = giac::helpon (std::string (command, strlen (command)),
                                   *_help, ENGLISH, _help->size ());

  /* If giac::helpon doesn't find an exact match, _aide.related will have best
     matches, which can be treated as search results. */
  if (_aide.syntax.find ("No help available for") == 0 && _aide.related.size () > 0)
    {
      _aide = giac::helpon (_aide.related.at (0).chaine,
                            *_help, ENGLISH, _help->size ());
    }
  return cc_aide_to_c (_aide);
}

void
giac_aide_delete (giac_aide *aide)
{
  free (aide->cmd_name);
  free (aide->explanation);

  for (int j = 0; j < aide->examples_length; j++)
    free (aide->examples[j]);
  free (aide->examples);

  for (int j = 0; j < aide->related_length; j++)
    free (aide->related[j]);
  free (aide->related);

  for (int j = 0; j < aide->synonyms_length; j++)
    free (aide->synonyms[j]);
  free (aide->synonyms);

  free (aide->syntax);

  free (aide);
}

giac_eval
giac_eval_string (const char   *str,
                  giac_context *ct)
{
  giac::context *_ct = reinterpret_cast<giac::context *> (ct);
  int level = giac::eval_level (_ct);

  giac::gen g (str, _ct);
  try
    {
      g = g.eval (level, _ct);
    }
  catch (std::exception &e)
    {
      std::cerr << e.what () << std::endl;
    }

  char *eval = strdup (g.print (_ct).c_str ());
  char *latex = strdup (giac::gen2tex (g, _ct).c_str ());

  return giac_eval { eval, latex };
}
