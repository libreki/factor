/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Factor CAS.
 *
 *  Factor CAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Factor CAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Factor CAS.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef CGIAC_H
#define CGIAC_H

#ifdef __cplusplus
extern "C"
{
#endif

  typedef void giac_context;
  typedef void giac_vector_aide;

  typedef struct
  {
    char *eval;
    char *latex;
  } giac_eval;

  /**
   * C struct representation of giac::aide class. Stores user documentation of
   * a particular Giac command.
   *
   * For giac::localized_strings only the English text is used, other languages
   * are not included.
   */
  typedef struct
  {
    char  *cmd_name;
    char  *explanation;

    int    examples_length;
    char **examples;

    int    related_length;
    char **related;

    int    synonyms_length;
    char **synonyms;

    char  *syntax;
  } giac_aide;

  /**
   * Creates a new Giac context pointer which stores all math context such as
   * variables and settings.
   *
   * @return giac::context pointer cast as a void pointer.
   *         Caller is responsible for freeing with giac_context_delete.
   */
  giac_context      *giac_context_new        (void);
  void               giac_context_delete     (giac_context *ct);

  /**
   * Reads the default Giac help file containing user documentation for all
   * Giac commands.
   *
   * @return std::vector<giac::aide> pointer cast as a void pointer.
   *         Caller is responsible for freeing with giac_vector_aide_delete.
   */
  giac_vector_aide  *giac_read_help          (void);
  void               giac_vector_aide_delete (giac_vector_aide *help);

  /**
   * Gets an array of all Giac commands by name given a vector of user
   * documentation of every command.
   *
   * @param help giac_vector_aide loaded with giac_read_help.
   *
   * @return Null-terminated array of C strings.
   *         Caller is responsible for freeing array but not strings.
   */
  const char       **giac_get_commands       (giac_vector_aide *help);

  /**
   * Gets user documentation on a particular Giac command.
   *
   * @param command Giac command to get help on.
   * @param help giac_vector_aide loaded with giac_read_help.
   *
   * @return Pointer to a new giac_aide struct.
   *         Caller is responsible for freeing with giac_aide_delete.
   */
  giac_aide         *giac_help_on            (const char       *command,
                                              giac_vector_aide *help);
  void               giac_aide_delete        (giac_aide *aide);

  giac_eval          giac_eval_string        (const char   *str,
                                              giac_context *ct);

#ifdef __cplusplus
}
#endif

#endif
