/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Factor CAS.
 *
 *  Factor CAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Factor CAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Factor CAS.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "factor-application.h"

#include "factor-index.h"
#include "factor-window.h"

struct _FactorApplication
{
  AdwApplication  parent;

  FactorWindow   *win;
  FactorIndex    *index;
};

G_DEFINE_TYPE (FactorApplication, factor_application, ADW_TYPE_APPLICATION);

static void
action_index (GSimpleAction *action,
              GVariant      *parameter,
              gpointer       app)
{
  gtk_window_present (GTK_WINDOW (FACTOR_APPLICATION (app)->index));
}

static void
action_about (GSimpleAction *action,
              GVariant      *parameter,
              gpointer       app)
{
  adw_show_about_window (GTK_WINDOW (FACTOR_APPLICATION (app)->win),
                         "application-icon", "org.codeberg.libreki.factor",
                         "application-name", "Factor CAS",
                         "website", "https://codeberg.org/libreki/factor",
                         "copyright", "Copyright © 2022-2023 libreki",
                         "license-type", GTK_LICENSE_GPL_3_0,
                         NULL);
}

static void
action_quit (GSimpleAction *action,
             GVariant      *parameter,
             gpointer       app)
{
  g_application_quit (G_APPLICATION (app));
}

static GActionEntry app_entries[] =
  {
    { "index", action_index, NULL, NULL, NULL },
    { "about", action_about, NULL, NULL, NULL },
    { "quit", action_quit, NULL, NULL, NULL },
  };

static void
factor_application_activate (GApplication *app)
{
  FactorApplication *self = FACTOR_APPLICATION (app);

  self->win = factor_window_new (FACTOR_APPLICATION (app));
  self->index = factor_index_new ();

  gtk_window_set_transient_for (GTK_WINDOW (self->index), GTK_WINDOW (self->win));

  g_action_activate (g_action_map_lookup_action (G_ACTION_MAP (self->win), "new-math-page"), NULL);
  gtk_window_present (GTK_WINDOW (self->win));
}

static void
factor_application_startup (GApplication *app)
{
  G_APPLICATION_CLASS (factor_application_parent_class)->startup (app);

  g_action_map_add_action_entries (G_ACTION_MAP (app),
                                   app_entries, G_N_ELEMENTS (app_entries),
                                   app);
}

static void
factor_application_shutdown (GApplication *app)
{
  FactorApplication *self = FACTOR_APPLICATION (app);

  g_object_unref (self->index);

  G_APPLICATION_CLASS (factor_application_parent_class)->shutdown (app);
}

static void
factor_application_init (FactorApplication *app)
{
#define ACCEL(a) ((const gchar *[2]) { a, NULL })

  gtk_application_set_accels_for_action (GTK_APPLICATION (app), "app.index", ACCEL ("F1"));
}

static void
factor_application_class_init (FactorApplicationClass *klass)
{
  G_APPLICATION_CLASS (klass)->activate = factor_application_activate;
  G_APPLICATION_CLASS (klass)->startup = factor_application_startup;
  G_APPLICATION_CLASS (klass)->shutdown = factor_application_shutdown;
}

FactorApplication *
factor_application_new (void)
{
  return g_object_new (FACTOR_TYPE_APPLICATION,
                       "application-id", "org.codeberg.libreki.factor",
                       "flags", G_APPLICATION_DEFAULT_FLAGS,
                       NULL);
}

giac_vector_aide *
factor_application_get_help (void)
{
  FactorApplication *self;

  self = FACTOR_APPLICATION (g_application_get_default ());
  return factor_index_get_help (self->index);
}
