/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Factor CAS.
 *
 *  Factor CAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Factor CAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Factor CAS.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef FACTOR_APPLICATION_H
#define FACTOR_APPLICATION_H

#include <adwaita.h>
#include <gtk/gtk.h>

#include <cgiac.h>

G_BEGIN_DECLS

#define FACTOR_TYPE_APPLICATION (factor_application_get_type ())
G_DECLARE_FINAL_TYPE (FactorApplication, factor_application, FACTOR, APPLICATION, AdwApplication)

FactorApplication *factor_application_new      (void);

giac_vector_aide  *factor_application_get_help (void);

G_END_DECLS

#endif
