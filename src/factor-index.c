/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Factor CAS.
 *
 *  Factor CAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Factor CAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Factor CAS.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "factor-index.h"

struct _FactorIndex
{
  AdwWindow         parent;

  giac_vector_aide *help;

  GtkWidget        *search_button;
  GtkWidget        *search_bar;
  GtkAdjustment    *index_vadjustment;
  GtkWidget        *index_list_box;

  GtkWidget        *command_label;
  GtkWidget        *explanation_label;
  GtkWidget        *syntax_label;
  GtkWidget        *examples_box;
  GtkWidget        *examples_bin;
  GtkWidget        *related_box;
  GtkWidget        *related_bin;
  GtkWidget        *synonyms_box;
  GtkWidget        *synonyms_bin;
};

G_DEFINE_TYPE (FactorIndex, factor_index, ADW_TYPE_WINDOW);

static void
set_list_box_section (GtkWidget  *box,
                      AdwBin     *bin,
                      char      **array,
                      int         length)
{
  GtkWidget *list_box;

  if (length == 0)
    {
      gtk_widget_set_visible (box, FALSE);
      return;
    }
  else
    {
      gtk_widget_set_visible (box, TRUE);
    }

  list_box = gtk_list_box_new ();
  gtk_widget_add_css_class (list_box, "boxed-list");
  gtk_widget_add_css_class (list_box, "rich-list");

  for (int i = 0; i < length; i++)
    {
      GtkWidget *row;

      row = gtk_list_box_row_new ();
      gtk_list_box_row_set_child (GTK_LIST_BOX_ROW (row),
                                  gtk_label_new (array[i]));
      gtk_list_box_append (GTK_LIST_BOX (list_box), row);
    }

  adw_bin_set_child (bin, list_box);
}

static void
index_row_selected (FactorIndex *index)
{
  GtkListBoxRow *index_row;
  const char *command;
  giac_aide *aide;

  index_row = gtk_list_box_get_selected_row (GTK_LIST_BOX (index->index_list_box));
  /* During shutdown the program will segfault if this function doesn't return.
     TODO: Remove signal during destruction? */
  if (!index_row)
    return;

  command = gtk_label_get_text (GTK_LABEL (gtk_list_box_row_get_child (index_row)));
  if (strlen (command) == 0)
    return;

  aide = giac_help_on (command, index->help);

  gtk_label_set_text (GTK_LABEL (index->command_label), command);
  gtk_label_set_text (GTK_LABEL (index->explanation_label), aide->explanation);
  gtk_label_set_text (GTK_LABEL (index->syntax_label), aide->syntax);

  set_list_box_section (index->examples_box, ADW_BIN (index->examples_bin),
                        aide->examples, aide->examples_length);
  set_list_box_section (index->related_box, ADW_BIN (index->related_bin),
                        aide->related, aide->related_length);
  set_list_box_section (index->synonyms_box, ADW_BIN (index->synonyms_bin),
                        aide->synonyms, aide->synonyms_length);

  giac_aide_delete (aide);
}

static void
search_changed (GtkSearchEntry *search_entry,
                FactorIndex    *index)
{
  const char *search;
  giac_aide *aide;

  search = gtk_editable_get_text (GTK_EDITABLE (search_entry));
  aide = giac_help_on (search, index->help);

  for (int i = 0; gtk_list_box_get_row_at_index (GTK_LIST_BOX (index->index_list_box), i); i++)
    {
      GtkListBoxRow *row;
      GtkLabel *label;

      row = gtk_list_box_get_row_at_index (GTK_LIST_BOX (index->index_list_box), i);
      label = GTK_LABEL (gtk_list_box_row_get_child (row));

      if (strcmp (aide->cmd_name, gtk_label_get_text (label)) == 0)
        {
          GtkAllocation row_allocation;

          gtk_list_box_select_row (GTK_LIST_BOX (index->index_list_box), row);

          gtk_widget_get_allocation (GTK_WIDGET (row), &row_allocation);
          gtk_adjustment_set_value (index->index_vadjustment, row_allocation.y);

          break;
        }
    }

  giac_aide_delete (aide);
}

static gboolean
close_index (GtkWidget *widget,
             GVariant  *args,
             gpointer   data)
{
  gtk_window_close (GTK_WINDOW (widget));

  return GDK_EVENT_STOP;
}

static void
factor_index_finalize (GObject *object)
{
  FactorIndex *self = FACTOR_INDEX (object);

  giac_vector_aide_delete (self->help);

  G_OBJECT_CLASS (factor_index_parent_class)->dispose (object);
}

static void
factor_index_init (FactorIndex *self)
{
  const char **commands;

  gtk_widget_init_template (GTK_WIDGET (self));

  self->help = giac_read_help ();
  commands = giac_get_commands (self->help);
  for (int i = 0; commands[i]; i++)
    {
      const char *command;
      GtkWidget *index_row;

      index_row = gtk_list_box_row_new ();
      command = commands[i];
      gtk_list_box_row_set_child (GTK_LIST_BOX_ROW (index_row), gtk_label_new (command));
      gtk_list_box_append (GTK_LIST_BOX (self->index_list_box), index_row);
    }
  g_free (commands);

  gtk_search_bar_set_key_capture_widget (GTK_SEARCH_BAR (self->search_bar), GTK_WIDGET (self));

  g_object_bind_property (self->search_button, "active",
                          self->search_bar, "search-mode-enabled",
                          G_BINDING_BIDIRECTIONAL);
}

static void
factor_index_class_init (FactorIndexClass *klass)
{
  GtkWidgetClass *widget_class;
  GObjectClass *object_class;

  widget_class = GTK_WIDGET_CLASS (klass);
  object_class = G_OBJECT_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/codeberg/libreki/factor/factor-index.ui");

  gtk_widget_class_bind_template_child (widget_class, FactorIndex, search_button);
  gtk_widget_class_bind_template_child (widget_class, FactorIndex, search_bar);
  gtk_widget_class_bind_template_child (widget_class, FactorIndex, index_vadjustment);
  gtk_widget_class_bind_template_child (widget_class, FactorIndex, index_list_box);

  gtk_widget_class_bind_template_child (widget_class, FactorIndex, command_label);
  gtk_widget_class_bind_template_child (widget_class, FactorIndex, explanation_label);
  gtk_widget_class_bind_template_child (widget_class, FactorIndex, syntax_label);
  gtk_widget_class_bind_template_child (widget_class, FactorIndex, examples_box);
  gtk_widget_class_bind_template_child (widget_class, FactorIndex, examples_bin);
  gtk_widget_class_bind_template_child (widget_class, FactorIndex, related_box);
  gtk_widget_class_bind_template_child (widget_class, FactorIndex, related_bin);
  gtk_widget_class_bind_template_child (widget_class, FactorIndex, synonyms_box);
  gtk_widget_class_bind_template_child (widget_class, FactorIndex, synonyms_bin);

  gtk_widget_class_bind_template_callback (widget_class, search_changed);
  gtk_widget_class_bind_template_callback (widget_class, index_row_selected);

  gtk_widget_class_add_binding (widget_class, GDK_KEY_Escape, 0, close_index, NULL);

  object_class->finalize = factor_index_finalize;
}

FactorIndex *
factor_index_new (void)
{
  return g_object_new (FACTOR_TYPE_INDEX, NULL);
}

giac_vector_aide *
factor_index_get_help (FactorIndex *self)
{
  return self->help;
}
