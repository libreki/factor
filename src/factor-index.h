/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Factor CAS.
 *
 *  Factor CAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Factor CAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Factor CAS.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef FACTOR_INDEX_H
#define FACTOR_INDEX_H

#include <adwaita.h>
#include <gtk/gtk.h>

#include <cgiac.h>

G_BEGIN_DECLS

#define FACTOR_TYPE_INDEX (factor_index_get_type ())
G_DECLARE_FINAL_TYPE (FactorIndex, factor_index, FACTOR, INDEX, AdwWindow)

FactorIndex      *factor_index_new      (void);

giac_vector_aide *factor_index_get_help (FactorIndex *self);

G_END_DECLS

#endif
