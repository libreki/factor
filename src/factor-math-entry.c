/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Factor CAS.
 *
 *  Factor CAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Factor CAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Factor CAS.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "factor-math-entry.h"

#include <cgiac.h>

#include "factor-application.h"

struct _FactorMathEntry
{
  GtkEntry   parent;

  GtkWidget *popover;
  guint      show_popover_timeout;
};

G_DEFINE_TYPE (FactorMathEntry, factor_math_entry, GTK_TYPE_ENTRY);

static gboolean
is_focused (FactorMathEntry *entry)
{
  return gtk_widget_has_focus (gtk_widget_get_first_child (GTK_WIDGET (entry)));
}

static void
remove_highlights (FactorMathEntry *entry)
{
  gtk_entry_set_attributes (GTK_ENTRY (entry), pango_attr_list_new ());
}

static void
add_highlight_at_index (PangoAttrList *list,
                        int            i)
{
  PangoAttribute *attr;

  attr = pango_attr_weight_new (PANGO_WEIGHT_BOLD);
  attr->start_index = i;
  attr->end_index = i + 1;
  pango_attr_list_insert (list, attr);
}

static int
find_matching_bracket (const char *text,
                       int         cursor)
{
  char open, close;
  int dir, match, bracket_count;

  open = text[cursor];
  switch (open)
    {
    case '(':
      close = ')';
      dir = 1;
      break;

    case '[':
      close = ']';
      dir = 1;
      break;

    case '{':
      close = '}';
      dir = 1;
      break;

    case ')':
      close = '(';
      dir = -1;
      break;

    case ']':
      close = '[';
      dir = -1;
      break;

    case '}':
      close = '{';
      dir = -1;
      break;

    default:
      return -1;
    }

  match = cursor;
  bracket_count = 1;
  while (bracket_count > 0)
    {
      match += dir;
      if (match < 0 || text[match] == '\0')
        return -1;

      if (text[match] == open)
        bracket_count++;
      else if (text[match] == close)
        bracket_count--;
    }

  return match;
}

static void
match_brackets (FactorMathEntry *entry)
{
  const char *text;
  PangoAttrList *list;
  int cursor, match;

  text = gtk_entry_buffer_get_text (gtk_entry_get_buffer (GTK_ENTRY (entry)));
  list = pango_attr_list_new ();

  cursor = gtk_editable_get_position (GTK_EDITABLE (entry));
  match = find_matching_bracket (text, cursor);

  /* Couldn't find a match to bracket in front of cursor, try to find one for
     behind the cursor if possible. */
  if (match < 0 && cursor > 0)
    {
      cursor--;
      match = find_matching_bracket (text, cursor);
    }

  if (match >= 0)
    {
      add_highlight_at_index (list, cursor);
      add_highlight_at_index (list, match);
    }

  gtk_entry_set_attributes (GTK_ENTRY (entry), list);
}

static void
help_on_input (FactorMathEntry *entry)
{
  const char *input;
  giac_vector_aide *help;
  giac_aide *aide;
  char *tooltip;

  input = gtk_entry_buffer_get_text (gtk_entry_get_buffer (GTK_ENTRY (entry)));
  if (strcmp (input, "") == 0)
    return;

  help = factor_application_get_help ();
  aide = giac_help_on (input, help);

  tooltip = g_strdup_printf ("%s%s\nRelated:", aide->syntax, aide->explanation);

  for (int i = 0; i < aide->related_length; i++)
    {
      char *tmp;

      tmp = g_strdup_printf ("%s %s", tooltip, aide->related[i]);
      g_free (tooltip);
      tooltip = tmp;
    }

  for (int i = 0; i < aide->examples_length; i++)
    {
      char *tmp;

      tmp = g_strdup_printf ("%s\nEx %d: %s", tooltip, i + 1, aide->examples[i]);
      g_free (tooltip);
      tooltip = tmp;
    }

  gtk_popover_set_child (GTK_POPOVER (entry->popover), gtk_label_new (tooltip));
  gtk_popover_popup (GTK_POPOVER (entry->popover));

  entry->show_popover_timeout = 0;
  giac_aide_delete (aide);
}

static void
remove_show_popover_timeout (guint *show_popover_timeout)
{
  if (*show_popover_timeout > 0)
    {
      g_source_remove (*show_popover_timeout);
      *show_popover_timeout = 0;
    }
}

static const int INTERVAL = 1000;

static void
cursor_moved (FactorMathEntry *entry)
{
  if (!is_focused (entry))
    return;

  gtk_popover_popdown (GTK_POPOVER (entry->popover));
  remove_show_popover_timeout (&entry->show_popover_timeout);
  match_brackets (entry);

  if (gtk_entry_get_text_length (GTK_ENTRY (entry)) > 0)
    {
      entry->show_popover_timeout =
        g_timeout_add_once (INTERVAL, (GSourceOnceFunc) help_on_input, entry);
    }
}

static void
focus_moved (FactorMathEntry *entry)
{
  if (is_focused (entry))
    {
      cursor_moved (entry);
      return;
    }

  gtk_popover_popdown (GTK_POPOVER (entry->popover));
  remove_show_popover_timeout (&entry->show_popover_timeout);
  remove_highlights (entry);
}

static void
factor_math_entry_dispose (GObject *object)
{
  FactorMathEntry *self = FACTOR_MATH_ENTRY (object);

  gtk_widget_unparent (self->popover);

  G_OBJECT_CLASS (factor_math_entry_parent_class)->dispose (object);
}

static void
factor_math_entry_init (FactorMathEntry *self)
{
  self->popover = gtk_popover_new ();
  gtk_widget_set_parent (self->popover, GTK_WIDGET (self));
  gtk_popover_set_autohide (GTK_POPOVER (self->popover), FALSE);

  g_signal_connect_swapped (self, "notify::cursor-position",
                            G_CALLBACK (cursor_moved), self);
  g_signal_connect_swapped (self, "notify::has-focus",
                            G_CALLBACK (focus_moved), self);
}

static void
factor_math_entry_class_init (FactorMathEntryClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = factor_math_entry_dispose;
}

FactorMathEntry *
factor_math_entry_new (void)
{
  return g_object_new (FACTOR_TYPE_MATH_ENTRY, NULL);
}
