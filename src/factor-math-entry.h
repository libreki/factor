/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Factor CAS.
 *
 *  Factor CAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Factor CAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Factor CAS.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef FACTOR_MATH_ENTRY_H
#define FACTOR_MATH_ENTRY_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define FACTOR_TYPE_MATH_ENTRY (factor_math_entry_get_type ())
G_DECLARE_FINAL_TYPE (FactorMathEntry, factor_math_entry, FACTOR, MATH_ENTRY, GtkEntry)

FactorMathEntry *factor_math_entry_new (void);

G_END_DECLS

#endif
