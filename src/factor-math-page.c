/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Factor CAS.
 *
 *  Factor CAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Factor CAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Factor CAS.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "factor-math-page.h"

#include <cgiac.h>

#include "factor-math-row.h"
#include "factor-window.h"

typedef struct
{
  guint timeout;
  int   row_top;
  int   row_bottom;
} FactorScrollData;

struct _FactorMathPage
{
  GtkBox            parent;

  giac_context     *ct;

  GtkAdjustment    *vadjustment;
  FactorScrollData  scroll;

  GtkWidget        *math_list_box;
  guint             add_row_handler_id;
};

G_DEFINE_TYPE (FactorMathPage, factor_math_page, GTK_TYPE_BOX);

typedef enum
  {
    PROP_GIAC_CONTEXT = 1,
    N_PROPERTIES
  } FactorMathPageProperty;

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL };

static void
factor_math_page_set_property (GObject      *object,
                               guint         property_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  FactorMathPage *self = FACTOR_MATH_PAGE (object);

  switch ((FactorMathPageProperty) property_id)
    {
    case PROP_GIAC_CONTEXT:
      self->ct = g_value_get_pointer (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
factor_math_page_get_property (GObject    *object,
                               guint       property_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  FactorMathPage *self = FACTOR_MATH_PAGE (object);

  switch ((FactorMathPageProperty) property_id)
    {
    case PROP_GIAC_CONTEXT:
      g_value_set_pointer (value, self->ct);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static int
vadjustment_top (GtkAdjustment *vadjustment)
{
  return gtk_adjustment_get_value (vadjustment);
}

static int
vadjustment_bottom (GtkAdjustment *vadjustment)
{
  return (gtk_adjustment_get_value (vadjustment)
          + gtk_adjustment_get_page_size (vadjustment));
}

/* TODO: Figure out appropriate value */
static const int INTERVAL = 3;
static const int INCREMENT = 1;

static gboolean
scroll_up (gpointer data)
{
  FactorMathPage *page = FACTOR_MATH_PAGE (data);

  gtk_adjustment_set_value (page->vadjustment,
                            gtk_adjustment_get_value (page->vadjustment) - INCREMENT);

  if (page->scroll.row_top >= vadjustment_top (page->vadjustment))
    {
      gtk_adjustment_set_value (page->vadjustment, page->scroll.row_top);
      page->scroll.timeout = 0;
      return G_SOURCE_REMOVE;
    }

  return G_SOURCE_CONTINUE;
}

static gboolean
scroll_down (gpointer data)
{
  FactorMathPage *page = FACTOR_MATH_PAGE (data);

  gtk_adjustment_set_value (page->vadjustment,
                            gtk_adjustment_get_value (page->vadjustment) + INCREMENT);

  if (page->scroll.row_bottom <= vadjustment_bottom (page->vadjustment))
    {
      gtk_adjustment_set_value (page->vadjustment,
                                page->scroll.row_bottom
                                - gtk_adjustment_get_page_size (page->vadjustment));
      page->scroll.timeout = 0;
      return G_SOURCE_REMOVE;
    }

  return G_SOURCE_CONTINUE;
}

static void
maybe_start_scrolling (FactorMathPage *page)
{
  FactorWindow *win;
  FactorMathRow *row;
  GtkAllocation row_allocation;

  win = factor_window_get ();
  /* This page isn't selected */
  if (page != factor_window_get_current_math_page (win))
    return;

  row = factor_math_row_get_from_focused_text (gtk_window_get_focus (GTK_WINDOW (win)));
  /* Selection isn't the entry of a row */
  if (!row)
    return;

  gtk_widget_get_allocation (GTK_WIDGET (row), &row_allocation);

  page->scroll.row_top = row_allocation.y;
  page->scroll.row_bottom = page->scroll.row_top + row_allocation.height;

  /* Widget hasn't been positioned yet */
  if (page->scroll.row_top < 0)
    return;

  /* If part of the selected FactorMathRow is off-screen, scroll up or down
     until it is completely visible */
  if (page->scroll.row_top < vadjustment_top (page->vadjustment))
    {
      if (page->scroll.timeout != 0)
        g_source_remove (page->scroll.timeout);

      page->scroll.timeout = g_timeout_add (INTERVAL, scroll_up, page);
    }
  else if (page->scroll.row_bottom > vadjustment_bottom (page->vadjustment))
    {
      if (page->scroll.timeout != 0)
        g_source_remove (page->scroll.timeout);

      page->scroll.timeout = g_timeout_add (INTERVAL, scroll_down, page);
    }
}

static void
add_math_row (FactorMathPage *page)
{
  FactorMathRow *last_row; /* The last row before the new one is added */
  FactorMathRow *new_row;

  last_row = FACTOR_MATH_ROW (gtk_widget_get_last_child (page->math_list_box));

  /* A new row is added only if the entry from the last row is submitted,
     disconnect old handler before connecting the signal from the newly added
     row */
  if (page->add_row_handler_id != 0)
    {
      g_signal_handler_disconnect (factor_math_row_get_entry (last_row),
                                   page->add_row_handler_id);
    }

  new_row = factor_math_row_new ();
  gtk_list_box_append (GTK_LIST_BOX (page->math_list_box), GTK_WIDGET (new_row));

  g_object_bind_property (page, "giac-context",
                          new_row, "giac-context",
                          G_BINDING_SYNC_CREATE);

  page->add_row_handler_id = g_signal_connect_swapped (factor_math_row_get_entry (new_row),
                                                       "activate", G_CALLBACK (add_math_row),
                                                       page);
}

static void
factor_math_page_finalize (GObject *object)
{
  FactorMathPage *self = FACTOR_MATH_PAGE (object);

  giac_context_delete (self->ct);

  if (self->scroll.timeout != 0)
    g_source_remove (self->scroll.timeout);

  G_OBJECT_CLASS (factor_math_page_parent_class)->dispose (object);
}

static void
factor_math_page_init (FactorMathPage *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->scroll.timeout = 0;
  self->add_row_handler_id = 0;
  add_math_row (self);

  g_signal_connect_swapped (self->vadjustment, "notify::upper",
                            G_CALLBACK (maybe_start_scrolling), self);
  g_signal_connect_swapped (factor_window_get (), "notify::focus-widget",
                            G_CALLBACK (maybe_start_scrolling), self);
}

static void
factor_math_page_class_init (FactorMathPageClass *klass)
{
  GtkWidgetClass *widget_class;
  GObjectClass *object_class;

  widget_class = GTK_WIDGET_CLASS (klass);
  object_class = G_OBJECT_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/codeberg/libreki/factor/factor-math-page.ui");

  gtk_widget_class_bind_template_child (widget_class, FactorMathPage, vadjustment);
  gtk_widget_class_bind_template_child (widget_class, FactorMathPage, math_list_box);

  object_class->set_property = factor_math_page_set_property;
  object_class->get_property = factor_math_page_get_property;
  object_class->finalize = factor_math_page_finalize;

  obj_properties[PROP_GIAC_CONTEXT] =
    g_param_spec_pointer ("giac-context",
                          "Giac context",
                          "",
                          G_PARAM_READWRITE);

  g_object_class_install_properties (object_class, N_PROPERTIES, obj_properties);
}

FactorMathPage *
factor_math_page_new (void)
{
  return g_object_new (FACTOR_TYPE_MATH_PAGE,
                       "giac-context", giac_context_new (),
                       NULL);
}
