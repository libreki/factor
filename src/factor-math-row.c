/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Factor CAS.
 *
 *  Factor CAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Factor CAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Factor CAS.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "factor-math-row.h"

#include <cgiac.h>

#include "factor-math-entry.h"
#include "factor-window.h"

struct _FactorMathRow
{
  GtkListBoxRow  parent;

  giac_context  *ct;

  GtkWidget     *paned;
  GtkWidget     *entry;
  GtkWidget     *stack;
  GtkWidget     *latex_output;
  GtkWidget     *spinner;
  GtkWidget     *menu;

  char          *eval;
  char          *latex;

  char          *tmp_file;

  GdkPixbuf     *latex_pixbuf;
};

static gboolean computing = FALSE;

G_DEFINE_TYPE (FactorMathRow, factor_math_row, GTK_TYPE_LIST_BOX_ROW);

typedef enum
  {
    PROP_GIAC_CONTEXT = 1,
    N_PROPERTIES
  } FactorMathRowProperty;

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL };

static void
factor_math_row_set_property (GObject      *object,
                              guint         property_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  FactorMathRow *self = FACTOR_MATH_ROW (object);

  switch ((FactorMathRowProperty) property_id)
    {
    case PROP_GIAC_CONTEXT:
      self->ct = g_value_get_pointer (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
action_copy (GSimpleAction *action,
             GVariant      *parameter,
             gpointer       row)
{
  GdkDisplay *display;
  GdkClipboard *clipboard;

  display = gdk_display_get_default ();
  clipboard = gdk_display_get_clipboard (display);

  gdk_clipboard_set_text (clipboard, FACTOR_MATH_ROW (row)->eval);
}

static void
action_copy_latex (GSimpleAction *action,
                   GVariant      *parameter,
                   gpointer       row)
{
  GdkDisplay *display;
  GdkClipboard *clipboard;

  display = gdk_display_get_default ();
  clipboard = gdk_display_get_clipboard (display);

  gdk_clipboard_set_text (clipboard, FACTOR_MATH_ROW (row)->latex);
}

static GActionEntry row_entries[] =
  {
    { "copy", action_copy, NULL, NULL, NULL },
    { "copy-latex", action_copy_latex, NULL, NULL, NULL },
  };

static const int GUCHAR_MAX = 255;
static const int N_CHANNELS = 4;

static void
invert_latex_output (FactorMathRow *row)
{
  guchar *pixels;
  int width, height;

  if (!row->latex_pixbuf)
    return;

  pixels = gdk_pixbuf_get_pixels (row->latex_pixbuf);
  width = gdk_pixbuf_get_width (row->latex_pixbuf);
  height = gdk_pixbuf_get_height (row->latex_pixbuf);

  for (int i = 0; i < (height * width * N_CHANNELS); i += N_CHANNELS)
    {
      pixels[i] = GUCHAR_MAX - pixels[i];
      pixels[i + 1] = GUCHAR_MAX - pixels[i + 1];
      pixels[i + 2] = GUCHAR_MAX - pixels[i + 2];
    }

  gtk_picture_set_pixbuf (GTK_PICTURE (row->latex_output), row->latex_pixbuf);
}

static const char *tex_preamble =
  "\\documentclass{article}\n"
  "\\usepackage{amsmath}\n"
  "\\usepackage{amsthm}\n"
  "\\usepackage{amssymb}\n"
  "\\usepackage{bm}\n"
  "\\pagenumbering{gobble}\n"
  "\\begin{document}\n"
  "$$";

static const char *tex_postamble =
  "$$\n"
  "\\end{document}";

/* The height of an SVG image of a single number generated using the LaTeX
   input */
static const int num_height = 11;

static const int default_row_height = 34;

static void
delete_tmp_files (FactorMathRow *row)
{
  static const char *files[5] = { "aux", "dvi", "log", "svg", "tex" };

  if (!row->tmp_file)
    return;

  for (int i = 0; i < 5; i++)
    {
      char *file;

      file = g_strdup_printf ("%s.%s", row->tmp_file, files[i]);
      remove (file);
      g_free (file);
    }
  g_free (row->tmp_file);
}

static void
display_svg_in_latex_output (GPid     pid,
                             gint     wait_status,
                             gpointer data)
{
  FactorMathRow *row;
  char *svg_path;
  GdkPixbuf *pixbuf;
  int width, height;
  AdwApplication *app;
  AdwStyleManager *style_manager;

  row = FACTOR_MATH_ROW (data);
  svg_path = g_strdup_printf ("%s.svg", row->tmp_file);

  /* Load without size to get the dimensions of the image */
  pixbuf = gdk_pixbuf_new_from_file (svg_path, NULL);
  width = gtk_widget_get_width (GTK_WIDGET (row));
  height = default_row_height * gdk_pixbuf_get_height (pixbuf) / num_height;

  /* Load image again to display it at an appropriate size */
  row->latex_pixbuf = gdk_pixbuf_new_from_file_at_size (svg_path, width, height, NULL);
  gtk_widget_set_size_request (row->latex_output, -1, height);

  app = ADW_APPLICATION (g_application_get_default ());
  style_manager = adw_application_get_style_manager (app);
  if (adw_style_manager_get_dark (style_manager)) /* Invert colors and display */
    invert_latex_output (row);
  else /* Display without inverting colors */
    gtk_picture_set_pixbuf (GTK_PICTURE (row->latex_output), row->latex_pixbuf);

  g_free (svg_path);
}

static void
compile_dvi_to_svg (GPid     pid,
                    gint     wait_status,
                    gpointer data)
{
  FactorMathRow *row;
  char *command;
  char **argv;
  GPid child_pid;

  row = FACTOR_MATH_ROW (data);
  command = g_strdup_printf ("dvisvgm %s.dvi --no-fonts -b 1 -o %s.svg",
                             row->tmp_file, row->tmp_file);

  g_shell_parse_argv (command, NULL, &argv, NULL);

  g_spawn_async (NULL, argv, NULL, G_SPAWN_SEARCH_PATH, NULL, NULL, &child_pid, NULL);
  g_child_watch_add (child_pid, display_svg_in_latex_output, row);

  g_free (command);
  g_strfreev (argv);
}

static void
create_tex_and_compile_to_dvi (GObject      *object,
                               GAsyncResult *res,
                               gpointer      data)
{
  FactorMathRow *row;
  GFile *tex_file;
  GFileIOStream *tex_io;
  GOutputStream *tex_out;
  char *command;
  char **argv;
  GPid child_pid;

  row = FACTOR_MATH_ROW (object);
  delete_tmp_files (row);

  tex_file = g_file_new_tmp ("factor_math.XXXXXX.tex", &tex_io, NULL);
  tex_out = g_io_stream_get_output_stream (G_IO_STREAM (tex_io));

  g_output_stream_write (tex_out, tex_preamble, strlen (tex_preamble), NULL, NULL);
  g_output_stream_write (tex_out, row->latex, strlen (row->latex), NULL, NULL);
  g_output_stream_write (tex_out, tex_postamble, strlen (tex_postamble), NULL, NULL);

  row->tmp_file = g_file_get_path (tex_file);
  row->tmp_file[strlen (row->tmp_file) - 4] = '\0';

  command = g_strdup_printf ("latex -output-directory=%s %s.tex ",
                             g_get_tmp_dir (), row->tmp_file);
  g_shell_parse_argv (command, NULL, &argv, NULL);

  g_spawn_async (NULL, argv, NULL, G_SPAWN_SEARCH_PATH, NULL, NULL, &child_pid, NULL);
  g_child_watch_add (child_pid, compile_dvi_to_svg, row);

  g_object_unref (tex_file);
  g_object_unref (tex_io);
  g_free (command);
  g_strfreev (argv);
}

static void
eval_entry (GTask        *task,
            gpointer      object,
            gpointer      data,
            GCancellable *cancellable)
{
  FactorMathRow *row;
  GtkEntry *entry;
  const char *input;
  giac_eval result;

  row = FACTOR_MATH_ROW (object);
  entry = GTK_ENTRY (data);

  gtk_stack_set_visible_child (GTK_STACK (row->stack), row->spinner);

  input = gtk_entry_buffer_get_text (gtk_entry_get_buffer (entry));
  result = giac_eval_string (input, row->ct);
  row->eval = result.eval;
  row->latex = result.latex;

  gtk_stack_set_visible_child (GTK_STACK (row->stack), row->latex_output);
  computing = FALSE;
}

static void
entry_submitted (GtkEntry      *entry,
                 FactorMathRow *row)
{
  GTask *task;
  GAction *action;

  /* If two rows are computing at the same time it may cause the program to
     crash. */
  if (!computing)
    {
      computing = TRUE;

      task = g_task_new (row, NULL, create_tex_and_compile_to_dvi, NULL);
      g_task_set_task_data (task, entry, NULL);
      g_task_run_in_thread (task, eval_entry);
      g_object_unref (task);
    }

  action = g_action_map_lookup_action (G_ACTION_MAP (factor_window_get ()), "down");
  g_action_activate (action, NULL);
}

static void
factor_math_row_finalize (GObject *object)
{
  FactorMathRow *self = FACTOR_MATH_ROW (object);

  delete_tmp_files (self);

  g_free (self->eval);
  g_free (self->latex);

  G_OBJECT_CLASS (factor_math_row_parent_class)->dispose (object);
}

static void
factor_math_row_init (FactorMathRow *self)
{
  GtkBuilder *builder;
  GMenuModel *menu;
  GActionGroup *action_group;
  AdwApplication *app;
  AdwStyleManager *style_manager;

  g_type_ensure (FACTOR_TYPE_MATH_ENTRY);
  gtk_widget_init_template (GTK_WIDGET (self));

  /* For some reason
     <property name="shrink-start-child">false</property>
     <property name="shrink-end-child">false</property>
     in the UI file doesn't work */
  gtk_paned_set_shrink_start_child (GTK_PANED (self->paned), FALSE);
  gtk_paned_set_shrink_end_child (GTK_PANED (self->paned), FALSE);

  self->eval = NULL;
  self->latex = NULL;
  self->tmp_file = NULL;
  self->latex_pixbuf = NULL;

  builder = gtk_builder_new_from_resource ("/org/codeberg/libreki/factor/factor-math-row-menu.ui");
  menu = G_MENU_MODEL (gtk_builder_get_object (builder, "menu"));
  gtk_menu_button_set_menu_model (GTK_MENU_BUTTON (self->menu), menu);
  g_object_unref (builder);

  action_group = G_ACTION_GROUP (g_simple_action_group_new ());
  g_action_map_add_action_entries (G_ACTION_MAP (action_group),
                                   row_entries, G_N_ELEMENTS (row_entries),
                                   self);
  gtk_widget_insert_action_group (GTK_WIDGET (self), "row", action_group);

  app = ADW_APPLICATION (g_application_get_default ());
  style_manager = adw_application_get_style_manager (app);
  g_signal_connect_swapped (style_manager, "notify::dark",
                            G_CALLBACK (invert_latex_output), self);
}

static void
factor_math_row_class_init (FactorMathRowClass *klass)
{
  GtkWidgetClass *widget_class;
  GObjectClass *object_class;

  widget_class = GTK_WIDGET_CLASS (klass);
  object_class = G_OBJECT_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/codeberg/libreki/factor/factor-math-row.ui");

  gtk_widget_class_bind_template_child (widget_class, FactorMathRow, paned);
  gtk_widget_class_bind_template_child (widget_class, FactorMathRow, entry);
  gtk_widget_class_bind_template_child (widget_class, FactorMathRow, stack);
  gtk_widget_class_bind_template_child (widget_class, FactorMathRow, latex_output);
  gtk_widget_class_bind_template_child (widget_class, FactorMathRow, spinner);
  gtk_widget_class_bind_template_child (widget_class, FactorMathRow, menu);

  gtk_widget_class_bind_template_callback (widget_class, entry_submitted);

  object_class->set_property = factor_math_row_set_property;
  object_class->finalize = factor_math_row_finalize;

  obj_properties[PROP_GIAC_CONTEXT] =
    g_param_spec_pointer ("giac-context",
                          "Giac context",
                          "",
                          G_PARAM_WRITABLE);

  g_object_class_install_properties (object_class, N_PROPERTIES, obj_properties);
}

FactorMathRow *
factor_math_row_new (void)
{
  return g_object_new (FACTOR_TYPE_MATH_ROW, NULL);
}

GtkEntry *
factor_math_row_get_entry (FactorMathRow *self)
{
  return GTK_ENTRY (self->entry);
}

FactorMathRow *
factor_math_row_get_from_focused_text (GtkWidget *focused)
{
  GtkWidget *parent;

  if (!GTK_IS_TEXT (focused))
    return NULL;

  for (parent = gtk_widget_get_parent (focused);
       parent != NULL;
       parent = gtk_widget_get_parent (parent))
    {
      if (FACTOR_IS_MATH_ROW (parent))
        return FACTOR_MATH_ROW (parent);
    }

  return NULL;
}
