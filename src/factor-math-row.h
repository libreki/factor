/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Factor CAS.
 *
 *  Factor CAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Factor CAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Factor CAS.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef FACTOR_MATH_ROW_H
#define FACTOR_MATH_ROW_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define FACTOR_TYPE_MATH_ROW (factor_math_row_get_type ())
G_DECLARE_FINAL_TYPE (FactorMathRow, factor_math_row, FACTOR, MATH_ROW, GtkListBoxRow)

FactorMathRow *factor_math_row_new                   (void);

GtkEntry      *factor_math_row_get_entry             (FactorMathRow *self);
/* Returns a pointer to the FactorMathRow that the focused GtkText belongs to
   or NULL if the focused widget is not the GtkText of the GtkEntry in
   FactorMathRow */
FactorMathRow *factor_math_row_get_from_focused_text (GtkWidget *focused);

G_END_DECLS

#endif
