/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Factor CAS.
 *
 *  Factor CAS is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Factor CAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Factor CAS.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "factor-window.h"

#include "factor-math-page.h"
#include "factor-math-row.h"

struct _FactorWindow
{
  AdwApplicationWindow  parent;

  GtkWidget            *menu;

  GtkWidget            *tab_view;
  gint                  pages;
};

G_DEFINE_TYPE (FactorWindow, factor_window, ADW_TYPE_APPLICATION_WINDOW);

static void
action_up (GSimpleAction *action,
           GVariant      *parameter,
           gpointer       win)
{
  GtkWidget *focused;
  FactorMathRow *row;
  FactorMathRow *prev_row;

  focused = gtk_window_get_focus (GTK_WINDOW (win));
  row = factor_math_row_get_from_focused_text (focused);
  if (row)
    {
      prev_row = FACTOR_MATH_ROW (gtk_widget_get_prev_sibling (GTK_WIDGET (row)));
      if (prev_row)
        {
          gtk_window_set_focus (GTK_WINDOW (win),
                                GTK_WIDGET (factor_math_row_get_entry (prev_row)));
        }
    }
}

static void
action_down (GSimpleAction *action,
             GVariant      *parameter,
             gpointer       win)
{
  GtkWidget *focused;
  FactorMathRow *row;
  FactorMathRow *next_row;

  focused = gtk_window_get_focus (GTK_WINDOW (win));
  row = factor_math_row_get_from_focused_text (focused);
  if (row)
    {
      next_row = FACTOR_MATH_ROW (gtk_widget_get_next_sibling (GTK_WIDGET (row)));
      if (next_row)
        {
          gtk_window_set_focus (GTK_WINDOW (win),
                                GTK_WIDGET (factor_math_row_get_entry (next_row)));
        }
    }
}

static void
action_new_math_page (GSimpleAction *action,
                      GVariant      *parameter,
                      gpointer       data)
{
  FactorWindow *win;
  FactorMathPage *page;
  AdwTabPage *tab_page;

  win = FACTOR_WINDOW (data);
  page = factor_math_page_new ();
  win->pages++;

  tab_page = adw_tab_view_append (ADW_TAB_VIEW (win->tab_view), GTK_WIDGET (page));
  adw_tab_view_set_selected_page (ADW_TAB_VIEW (win->tab_view), tab_page);
  adw_tab_page_set_title (tab_page, g_strdup_printf ("Page %d", win->pages));
}

static GActionEntry win_entries[] =
  {
    { "up", action_up, NULL, NULL, NULL },
    { "down", action_down, NULL, NULL, NULL },
    { "new-math-page", action_new_math_page, NULL, NULL, NULL }
  };

static void
new_math_page_clicked (FactorWindow *win)
{
  g_action_activate (g_action_map_lookup_action (G_ACTION_MAP (win), "new-math-page"), NULL);
}

static void
factor_window_init (FactorWindow *self)
{
  GtkBuilder *builder;
  GMenuModel *menu;
  GApplication *app;

  gtk_widget_init_template (GTK_WIDGET (self));

  self->pages = 0;

  builder = gtk_builder_new_from_resource ("/org/codeberg/libreki/factor/factor-menu.ui");
  menu = G_MENU_MODEL (gtk_builder_get_object (builder, "menu"));
  gtk_menu_button_set_menu_model (GTK_MENU_BUTTON (self->menu), menu);
  g_object_unref (builder);

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   win_entries, G_N_ELEMENTS (win_entries),
                                   self);

#define ACCEL(a) ((const gchar *[2]) { a, NULL })

  app = g_application_get_default ();
  gtk_application_set_accels_for_action (GTK_APPLICATION (app), "win.up", ACCEL ("Up"));
  gtk_application_set_accels_for_action (GTK_APPLICATION (app), "win.down", ACCEL ("Down"));
}

static void
factor_window_class_init (FactorWindowClass *klass)
{
  GtkWidgetClass *widget_class;

  widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/codeberg/libreki/factor/factor-window.ui");

  gtk_widget_class_bind_template_child (widget_class, FactorWindow, menu);
  gtk_widget_class_bind_template_child (widget_class, FactorWindow, tab_view);

  gtk_widget_class_bind_template_callback (widget_class, new_math_page_clicked);
}

FactorWindow *
factor_window_new (FactorApplication *app)
{
  return g_object_new (FACTOR_TYPE_WINDOW, "application", app, NULL);
}

FactorWindow *
factor_window_get (void)
{
  GtkApplication *app;

  app = GTK_APPLICATION (g_application_get_default ());
  return FACTOR_WINDOW (gtk_application_get_active_window (app));
}

FactorMathPage *
factor_window_get_current_math_page (FactorWindow *self)
{
  AdwTabPage *tab_page;

  tab_page = adw_tab_view_get_selected_page (ADW_TAB_VIEW (self->tab_view));
  return FACTOR_MATH_PAGE (adw_tab_page_get_child (tab_page));
}
